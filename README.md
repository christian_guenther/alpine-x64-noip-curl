# [alpine-x64-noip-curl](https://hub.docker.com/r/forumi0721alpinex64/alpine-x64-noip-curl/)
[![](https://images.microbadger.com/badges/version/forumi0721alpinex64/alpine-x64-noip-curl.svg)](https://microbadger.com/images/forumi0721alpinex64/alpine-x64-noip-curl "Get your own version badge on microbadger.com") [![](https://images.microbadger.com/badges/image/forumi0721alpinex64/alpine-x64-noip-curl.svg)](https://microbadger.com/images/forumi0721alpinex64/alpine-x64-noip-curl "Get your own image badge on microbadger.com") [![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpinex64/alpine-x64-noip-curl.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinex64/alpine-x64-noip-curl/) [![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpinex64/alpine-x64-noip-curl.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinex64/alpine-x64-noip-curl/)



----------------------------------------
#### Description
* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : x64
* Base Image   : [forumi0721/alpine-x64-base](https://hub.docker.com/r/forumi0721/alpine-x64-base/)
* Appplication : [noip-curl-updater](https://www.npmjs.com/package/noip-curl-updater)



----------------------------------------
#### Run
```sh
docker run -d \
           -e USER_NAME=username \
           -e USER_PASSWD=passwd \
           -e DOMAIN=<domain> \
           -e FREQUENCY=<domain> \
           forumi0721alpinex64/alpine-x64-noip-curl:latest
```



----------------------------------------
#### Usage
* Nothing to do. This script will update NoIP automatically.



----------------------------------------
#### Docker Options
| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports
| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Volumes
| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Environment Variables
| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| USER_NAME          | Login username                                   |
| USER_PASSWD        | Login password                                   |
| USER_EPASSWD       | Login password (base64)                          |
| DOMAIN             | Target NoIP domain                               |
| FREQUENCY          | Update frequency (default : 12)                  |



----------------------------------------
* [forumi0721alpinex64/alpine-x64-noip-curl](https://hub.docker.com/r/forumi0721alpinex64/alpine-x64-noip-curl/)
* [forumi0721alpinearmhf/alpine-armhf-noip-curl](https://hub.docker.com/r/forumi0721alpinearmhf/alpine-armhf-noip-curl/)
* [forumi0721alpineaarch64/alpine-aarch64-noip-curl](https://hub.docker.com/r/forumi0721alpineaarch64/alpine-aarch64-noip-curl/)

